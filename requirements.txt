Django==1.8
requests>=2.7.0
rsa>=3.2
jsonfield==1.0.3
arrow==0.7.0
pytz==2015.6
