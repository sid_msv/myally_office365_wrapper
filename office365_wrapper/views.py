# Copyright (c) Microsoft. All rights reserved. Licensed under the MIT license. See full license at the bottom of this file.
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
import logging
from office365_wrapper.clientcredhelper import get_client_cred_authorization_url, get_access_token
from office365_wrapper.o365service import make_api_call
from models import Office365AccountCredentials
from django.utils.decorators import method_decorator
import clientcredhelper
import arrow
import json
import pytz
import time

api_endpoint = "https://outlook.office365.com/api/v1.0"
# Used for debug logging
logger = logging.getLogger('office365_wrapper')

# Home page (just shows a link to admin consent)
def home(request):
  redirect_url = request.build_absolute_uri(reverse('office365_wrapper:consent'))
  sign_in_url = get_client_cred_authorization_url(redirect_url, 'https://outlook.office365.com/')
  return HttpResponse('<a href="' + sign_in_url + '">Click here to sign in with an admin account</a>')

# Consent action. Marked CSRF exempt because it is called from Azure
@csrf_exempt
def consent(request):
  # We're expecting a POST with form data
  if request.method == "POST":
	# The ID token we requested is included as the "id_token" form field
	id_token = request.POST["id_token"];
	redirect_url = request.build_absolute_uri(reverse('office365_wrapper:consent'))
	# Get an access token
	access_token = get_access_token(id_token, redirect_url, 'https://outlook.office365.com/')
	account = Office365AccountCredentials.objects.create(id_token = id_token, credentials = access_token)
	#print(access_token)
	if (access_token['access_token']):
	  # Redirect to the mail page
	  return HttpResponse("successfully connected")
	else:
	  return HttpResponse("ERROR: " + access_token['error_description'])
  else:
	return HttpResponse("successfully connected")
#SQLiteDB, CiscoContainer, Certificate Authentification
class GetEvents(View):
	@method_decorator(csrf_exempt)
	def get(self, request):
		context = { 'user_email': 'NONE' }
		return render(request, 'office365_wrapper/events.html', context)

	@method_decorator(csrf_exempt)
	def post(self, request):
		email = request.POST['user_email']
		start = request.POST['start']
		end = request.POST['end']
		new_account = Office365Adapter(email = email);
		events = new_account.get_events(start, end);
		context = new_account.free_busy(events);
		if context:
			status = True;
		else:
			status = False
		return JsonResponse({"success":status, "Events":context});

def refresh_account_token():
	logger.info("Refreshing token")
	office_account_creds = Office365AccountCredentials.objects.all().first()
	creds = office_account_creds.credentials
	id_token = office_account_creds.id_token
	success = False
	if "expires_on" not in creds or int(time.time()) > int(creds["expires_on"]):
		logger.info("access token expired or expires_on key not available")
		#req_refresh = requests.post(request_url,data=data,headers=headers)
		#response_refresh = req_refresh.json()
		#redirect_url = settings.OFFICE365_SA_REDIRECT_URI
		redirect_url = 'http://localhost:8000/office365/connect/app/oauth2callback'
		access_token = clientcredhelper.get_access_token(id_token, redirect_url, 'https://outlook.office365.com/')
		office_account_creds.credentials = access_token
		office_account_creds.save()
		logger.info("received new access token")
		success=True
	else:
		logger.info("access token is still valid")
		#print response_refresh
	return success

class Office365Adapter:
	def __init__(self, email):
		self.email = email
		self.type = "OFFICE365"

	def get_events(self, start, end):
		logger.debug("{},{},{}".format(self.email,start,end))
		cal_events = []
		skip_events = 0

		# Message to be returned
		status = False
		message = ''

		parameters = {
		"startdatetime":start,
		"enddatetime": end,
		"$top":50,
		"$skip":skip_events,
		}
		try:
			token_refreshed=refresh_account_token()
			account_level_creds = Office365AccountCredentials.objects.all().first()
			token = account_level_creds.credentials['access_token']

			# Get Calendars:
			base_url = "{0}/users('{1}')".format(api_endpoint,self.email)
			get_calendars = "{0}/calendars".format(base_url)
			get_primary_calendar = "{0}/calendar".format(base_url)
			r = make_api_call('GET', get_calendars, token)
			#r = make_api_call('GET', "{0}/users('{1}')/calendar".format(api_endpoint, email), token)
			print(r);
			if 'error' in r.json():
				logger.error('Error:(%s) - %s', self.email, str(r.json()['error']['message']))
			cals = r.json()['value']
			#print cals

			r2 = make_api_call('GET', get_primary_calendar, token)
			if 'error' in r2.json():
				logger.error('Error:(%s) - %s', self.email, str(r2.json()['error']['message']))
				primary_cal = None
			else:
				primary_cal = r2.json()

			# Following code is used for fetching either only primary calendar or all local calendars owned by the customer
			filtered_cals =[]
			primary_calendar_list = ['Calendar','Kalender','Calendrier'] ## To identify if it is a primary calendar
			try:
				if primary_cal:
					filtered_cals.append(primary_cal)
				else:
					for cal in cals:
						if cal['Name'] in primary_calendar_list:
							filtered_cals.append(cal)
			except ObjectDoesNotExist:
				for cal in cals:
					if cal['Name'] in primary_calendar_list:
						filtered_cals.append(cal)

			for cal in filtered_cals:
				logger.info('building url for calendar - %s' % cal['Name'])
				#print token
				get_events = '{0}/calendars/{1}/calendarview'.format(base_url,cal['Id'])
				r = make_api_call('GET', get_events, token, parameters)
				#print r
				#print r.json()
				cal_events = cal_events + r.json()['value']
				#print cal_events
				'''
				Get_events api returns maximum of 50 events. If there are more than fifty events
				, the response includes an odata.nextLink property
				Keep making the call till all events are retrived'''
				while "@odata.nextLink" in r.json():
					next_link = r.json()['@odata.nextLink']
					next_link = urllib.unquote(next_link)
					parsed_link = urlparse(next_link)
					skip_events = int(parse_qs(parsed_link.query)['$skip'][0])
					'''Update parameters'''
					parameters["$skip"] = skip_events
					r = make_api_call('GET', get_events, token, parameters)
					cal_events = cal_events+r.json()['value']
				message = "events successfully retrived\n"
				logger.info(message)
				status = True
		except Exception as e:
			logger.exception(str(e.message))
			message = "Failed to get Events\n"
			logger.error(message)
			status = False
			cal_events = []
		return cal_events
		#return {"success":status,"message": message, "Events":cal_events}

	def free_busy(self, cal_events):
		req = ['End', 'EndTimeZone', 'Start', 'StartTimeZone', 'IsAllDay', 'IsCancelled', 'ShowAs'];
		FB = [];
		for event in cal_events:
			freebusy = {x: event[x] for x in req};
			Fb = {};
			if not event['IsCancelled']:
				if not event['EndTimeZone'] == event['StartTimeZone']:
					event['End'] = pytz.utc.localize(event['End'], is_dst = None).astimeZone(event['StartTimeZone']);
				Fb['Status'] = event['ShowAs'];
				Fb['Start'] = event['Start'];
				Fb['End'] = event['End'];
				Fb['TimeZone'] = event['StartTimeZone'];
				if event['IsAllDay']:
					Fb['End'] = 'All Day'
			#print Fb;
			FB.append(Fb);
		context = {
					'user_email': self.email,
					'messages': FB,
				  }
		return context
