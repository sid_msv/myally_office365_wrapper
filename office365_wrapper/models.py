from django.db import models
from jsonfield import JSONField

class Office365AccountCredentials(models.Model):
    id_token = models.TextField(null=True, blank=True)
    credentials = JSONField(null=True, blank=True)
    def __unicode__(self):
        return self.account.name
