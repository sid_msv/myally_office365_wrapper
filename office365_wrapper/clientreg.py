# Copyright (c) Microsoft. All rights reserved. Licensed under the MIT license. See full license at the bottom of this file.
import os
# The client ID (register app in Azure AD to get this value)
id = '0fb2796d-6aa7-4ba1-99e4-b26041572500'

# The full path to your private key file. This file should be in RSA
# private key format.
cert_file_path = os.path.join(os.path.dirname(__file__), 'certificates/o365app_dev.pem')

# The thumbprint for the certificate that corresponds to your private key.
cert_file_thumbprint = 'cxUWDfk9o8GT2Cv3l7qe18IUPi8='

class client_registration:
    @staticmethod
    def client_id():
      return id

    @staticmethod
    def cert_path():
      return cert_file_path

    @staticmethod
    def cert_thumbprint():
      return cert_file_thumbprint

# MIT License:

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# ""Software""), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED ""AS IS"", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
