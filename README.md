# Python Client Credentials Sample #

This is a very rough sample illustrating how to implement the client credential OAuth2 flow in a Python/Django app. The app allows an administrator to logon and give consent, and then allows the user to view the free-busy information during a timeframe, of any user in the organization.

## Required software ##

- [Python 3.4.2](https://www.python.org/downloads/)
- [Django 1.7.1](https://docs.djangoproject.com/en/1.7/intro/install/)
- [Requests: HTTP for Humans](http://docs.python-requests.org/en/latest/)
- [Python-RSA](http://stuvel.eu/rsa)

## Running the sample ##

It's assumed that you have Python and Django installed before starting. Windows users should add the Python install directory and Scripts subdirectory to their PATH environment variable.

1. Download or fork the sample project.
1. Open your command prompt or shell to the directory where `manage.py` is located.
1. If you can run BAT files, run setup_project.bat. If not, run the three commands in the file manually. The last command prompts you to create a superuser, which you'll use later to logon.
1. Install libraries from requirements.txt
1. [Register the app in Azure Active Directory](https://github.com/jasonjoh/office365-azure-guides/blob/master/RegisterAnAppInAzure.md). The app should be registered as a web app with a Sign-on URL of "http://127.0.0.1:8000/", and should be given the permission to "Calendar access for all calendars in the organization", which is available in the "Application Permissions" dropdown.
1. An X509 certificate has been configured for this app
1. The private key has been extracted in RSA format from your certificate and saved to a PEM file. (I used OpenSSL to do this).
    `openssl pkcs12 -in <path to PFX file> -nodes -nocerts -passin pass:<cert password> | openssl rsa -out appcert.pem`
1. Edit the `.\office365_wrapper\clientreg.py` file.
	1. Copy the client ID for your app obtained during app registration and paste it as the value for the `id` variable.
	1. Enter the full path to the PEM file containing the RSA private key as the value for the `cert_file_path` variable.
	1. Copy the thumbprint value of your certificate (same value used for the `customKeyIdentifier` value in the application manifest) and paste it as the value for the `cert_file_thumbprint` variable.
	1. Save the file.
1. Start the development server: `python manage.py runserver`
1. You should see output like:
    `Performing system checks...`

    `System check identified no issues (0 silenced).`
    `December 18, 2014 - 12:36:32`
    `Django version 1.7.1, using settings 'myally_office365_wrapper.settings'`
    `Starting development server at http://127.0.0.1:8000/`
    `Quit the server with CTRL-BREAK.`
1. Use your browser to go to http://127.0.0.1:8000/.
1. You should now be prompted to login with an adminstrative account. Click the link to do so and login with an Office 365 tenant administrator account. On succesful login, you'll see the message "successfully connected".
2. Now you can send a post request with the body in the form:
{"user_email":'the email id', "start":"the_start_date_of_your_timeframe", "end":"the_end_date_of_your_timeframe"}
Also Headers in the request must include X-CSRFToken and cookie.
